import styled from 'styled-components'

export const PodsContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;

    width: ${props => props.isMobile ? '100vw' : '50vw'}

    .filterSelecter {
        margin: 6px;
    }

    .filterResetButton {
        margin: 6px;
    }
`;

export const PodsTable = styled.table`

    border-collapse: collapse;

    .dateHeadCell{
        width: 50%;
    }

    thead{
        tr:nth-child(1) {background: #a1edcf};
        th{
            background: #57c9a4;
        }
    }

    tbody{
        text-align: center;
        tr:nth-child(even) {background: #a1edcf};
        tr:nth-child(odd) {background: #9dd2d7};
    }
`