import React, { useEffect, useState } from 'react';
import { getPod } from '../api/pod.api';
import { EnergyCategory } from '../model/Energies';
import { PodElectriqueEntry, PodEntry, PodGasEntry, PodInfo } from '../model/Pods.type';
import { compare, distinct } from '../utils/commons.utils';
import { displayDate } from '../utils/dates.utils';
import { ReactComponent as ArrowUpIcon } from '../assets/icons/arrow-up-short.svg';
import { ReactComponent as ArrowDownIcon } from '../assets/icons/arrow-down-short.svg';
import { ReactComponent as ArrowLeftIcon } from '../assets/icons/arrow-left-short.svg';
import { PodsContainer, PodsTable } from './podDisplay.styled';
//Will be calculated on page creation, won't refresh if device is switched using browser devTool
import { isMobile } from 'react-device-detect';

type OwnProps = {
    podData: PodInfo
}
 
function PodDisplay(props: OwnProps) {

    const [pod, setPod] = useState<PodElectriqueEntry[] | PodGasEntry[]>([]);
    const [dateFilter, setDateFilter] = useState<number | undefined>();
    const [orderFilter, setOrderFilter] = useState<{ field: string, ascend: boolean }>(
        { field: "date", ascend: false}
    );

    function sortPodList(podList: PodElectriqueEntry[] | PodGasEntry[]) {
        if(orderFilter.field === "") {
            return podList;
        }
        else if(orderFilter.field === "date") {
            return podList.sort((a,b) => compare(new Date(a.date).getTime(), new Date(b.date).getTime(), orderFilter.ascend))
        } else {
            //@ts-ignore
            return podList.sort((a,b) => compare(a[orderFilter.field], b[orderFilter.field], orderFilter.ascend))
        }
    }

    function handleDateFilterChange(event: React.ChangeEvent<HTMLSelectElement>) {
        setDateFilter(event.target.value !== "" ? Number(event.target.value) : undefined);
    }

    function handleOrderFilterChange(fieldToChange: string){
        setOrderFilter(prev => ({ field: fieldToChange, ascend: prev.field === fieldToChange? !prev.ascend : true }))
    }

    function displayOrderingFilterArrow(field: string) {
        if( field === orderFilter.field ){
            return orderFilter.ascend ? <ArrowUpIcon/> : <ArrowDownIcon/>
        } else {
            return <ArrowLeftIcon/>
        }
    }

    function displayElecPod(pod: PodElectriqueEntry[]) {
        return (
            <PodsTable>
                <thead>
                    <tr>
                        <th className="dateHeadCell" onClick={ () => handleOrderFilterChange("date") }>Date { displayOrderingFilterArrow("date") }</th>
                        <th onClick={ () => handleOrderFilterChange("valueHP") }>Conso HP (en kWh) { displayOrderingFilterArrow("valueHP") }</th>
                        <th onClick={ () => handleOrderFilterChange("valueHC") }>Conso HC (en kWh) { displayOrderingFilterArrow("valueHC") }</th>
                    </tr>
                </thead>
                <tbody>
                    { pod.map(entry => 
                        <tr key={entry.id}>
                            <td>{displayDate(entry.date)}</td>
                            <td>{entry.valueHP}</td>
                            <td>{entry.valueHC}</td>
                        </tr>
                    )}
                </tbody>
            </PodsTable>
        )
    }

    function displayGasPod(pod: PodGasEntry[]) {
        return(
            <PodsTable>
                <thead>
                    <th className="dateHeadCell" onClick={ () => handleOrderFilterChange("date") }>Date { displayOrderingFilterArrow("date") }</th>
                    <th onClick={ () => handleOrderFilterChange("value") }>Conso en kWh { displayOrderingFilterArrow("value") }</th>
                </thead>
                <tbody>
                    { pod.map(entry => 
                        <tr key={entry.id}>
                            <td>{displayDate(entry.date)}</td>
                            <td>{entry.value}</td>
                        </tr>
                    )}
                </tbody>
            </PodsTable>
        )
    }

    function displayPod() {
        //@ts-ignore
        let podToDisplay = dateFilter ? pod.filter((entry: PodEntry) => new Date(entry.date).getFullYear() === dateFilter) : pod;
        podToDisplay = sortPodList(podToDisplay);

        switch(props.podData.energy) {
            case EnergyCategory.ELEC :
                //@ts-ignore
                return displayElecPod(podToDisplay);

            case EnergyCategory.GAS :
                //@ts-ignore
                return displayGasPod(podToDisplay);

            default :
                return <></>;
        }
    }

    useEffect(() => {
        getPod(
            props.podData.pointOfDelivery,
            (response) => {
                setPod(response);
            }, 
            () => {
                alert("An error occured while fetching datas from API")
            }
        )
    },[props.podData.pointOfDelivery])

    return (
        <PodsContainer isMobile={isMobile}>
            <select className='filterSelecter' value={ dateFilter } onChange={ e => handleDateFilterChange(e) }>
                <option value={ "" } >aucune année</option>
                { pod.map(entry => new Date(entry.date).getFullYear())
                    .filter(distinct)
                    .map(year => 
                        <option key={ year } value={ year }>
                            { year }
                        </option>
                    )
                }
            </select>
            <button className='filterResetButton' onClick={ () => setOrderFilter({ field: "", ascend:false })}>
                Réinitialiser les tris
            </button>
            { displayPod() }
        </PodsContainer>
    );
}

export default React.memo(PodDisplay);