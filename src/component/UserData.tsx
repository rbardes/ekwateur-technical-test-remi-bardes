import { type } from 'os';
import React, { useEffect, useState } from 'react';
import { getPodsIds } from '../api/pod.api';
import { EnergyCategory } from '../model/Energies';
import { PodInfo } from '../model/Pods.type';
import { ReactComponent as GasIcon } from '../assets/icons/cloud-fog2.svg';
import { ReactComponent as GasIconFill } from '../assets/icons/cloud-fog2-fill.svg';
import { ReactComponent as Lightning } from '../assets/icons/lightning-charge.svg';
import { ReactComponent as LightningFill } from '../assets/icons/lightning-charge-fill.svg';
import PodDisplay from './PodDisplay';
import {IconsContainer} from './userData.styled.js';

const IconStyle = {
    width: '5vh', 
    height: '5vh',
    margin: '1vh',
    cursor: 'pointer',
    backgroundColor: '#a1edcf',
    borderRadius: '50%',
}

function UserData(){

    const [podList, setPodList] = useState<PodInfo[]> ([]);
    const [podTypeSelected, setPodTypeSelected] = useState<EnergyCategory> (EnergyCategory.NONE);
    const [podToDisplay, setPodToDisplay] = useState<PodInfo | undefined>(undefined);

    function handlePodTypeSelect(type:EnergyCategory) {
        setPodTypeSelected(type);
    }

    useEffect(() => {
        getPodsIds(
            (response) => {
                setPodList(response);
            }, 
            () => {
                alert("An error occured while fetching datas from API")
            }
        )
    },[])

    useEffect(() => {
        if(podTypeSelected === EnergyCategory.NONE) {
             setPodToDisplay(undefined)
        } else {
            setPodToDisplay(podList.find(pod => pod.energy === podTypeSelected))
        }
    },[podList, podTypeSelected])

    return (
        <div>
            <IconsContainer>
                { podTypeSelected === EnergyCategory.ELEC ?
                    <LightningFill style={ IconStyle } onClick={() => handlePodTypeSelect(EnergyCategory.ELEC)}/>
                    : <Lightning style={ IconStyle } onClick={() => handlePodTypeSelect(EnergyCategory.ELEC)}/>
                }
                { podTypeSelected === EnergyCategory.GAS ?
                    <GasIconFill style={ IconStyle } onClick={() => handlePodTypeSelect(EnergyCategory.GAS)}/>
                    : <GasIcon style={ IconStyle } onClick={() => handlePodTypeSelect(EnergyCategory.GAS)}/>
                }
            </IconsContainer>
            { podToDisplay &&
                <PodDisplay
                    podData={ podToDisplay }
                />
            }
        </div>
    )
}

export default React.memo(UserData);