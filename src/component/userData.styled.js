import styled from 'styled-components'

export const IconsContainer = styled.div`
    width: 100vw;
    height:8 vh;
    display: flex;
    flex-direction: row;
    justify-content: center;
`;