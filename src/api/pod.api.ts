


const POD_URL = "https://61b0dd073c954f001722a6c4.mockapi.io/test-react"

export function getPodsIds(
    onSuccess: (response: any) => void, 
    onFailure: (error: Error) => void
    ) {
    fetch(
        `${POD_URL}/meters`
    ).then(response => response.json())
        .then(data => onSuccess(data))
        .catch(error => onFailure(error));
}

export function getPod(
    data: number,
    onSuccess: (response: any) => void, 
    onFailure: (error: Error) => void
    ) {
    fetch(
        `${POD_URL}/${data}`
    ).then(response => response.json())
        .then(data => onSuccess(data))
    .catch(error => onFailure(error));
}