/**
 * 
 * @param element the element
 * @param index the index of the element
 * @param array the array of all elements
 * @returns `true` if the element has the first index of the array (usefull to filter distinct element from an array)
 */
export function distinct<E>(element: E, index: number, array: E[]): boolean {
    return array.indexOf(element) === index;
}

export function compare(a: number, b:number, ascendingOrder:boolean):number {
    return ascendingOrder ? a-b : b-a;
}