export enum EnergyCategory {
    NONE = 'none',
    ELEC = 'electricity',
    GAS = 'gas'
}