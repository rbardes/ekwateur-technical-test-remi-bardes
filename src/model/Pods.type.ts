export interface PodInfo {
    createdAt: Date,
    energy: string,
    pointOfDelivery: number
}

export interface PodEntry {
    date : Date,
    id: number
}

export interface PodGasEntry extends PodEntry {
    value: number
}

export interface PodElectriqueEntry extends PodEntry {
    valueHP: number,
    valueHC: number,
}